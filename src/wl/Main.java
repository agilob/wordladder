package wl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Main class of the application, we execute everything here.
 * Class contains main() method.
 *
 */
public class Main {

  /**
   * Main method where everything happens.
   *
   * @param args Arguments from command line.
   */
  public static void main (String[] args) {

    InputStreamReader streamReader = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(streamReader);

    ArrayList dictionary = new ArrayList<>();

    String start = "", //must be initialized
            stop = "",
            fileName = "",
            mode = "";

    FileReader fr;
    WordLadder wl;

    if (args.length == 4) {

      fileName = args[0]; //read path to file
      fr = new FileReader(fileName); //create file reader instance
      dictionary = fr.getDictionary(); //load file to dictionary

      mode = args[1]; //second parameter is mode
      start = args[2]; //third parameter is start word
      stop = args[3]; //fourth parameter is end word

      wl = new WordLadder(start, stop, dictionary); //create graph instance

      if (mode.equals("1") && checkStrings(start, null, dictionary)) {
        //word ladder from start word
        wl.wordLadder(Integer.parseInt(stop));
      } else if (mode.equals("2") && checkStrings(start, stop, dictionary)) {
        //generate word ladder from start to end
        wl.tree();
      } //else if

    } else if (args.length
            == 0) {

      try {

        System.out.println("Type 1 for generation mode"
                + "\nor 2 for tree word-ladder mode:");
        mode = buffer.readLine();

        System.out.println("Provide full path to the dictionary: ");

        fileName = buffer.readLine();

        System.out.println("Provide first word: ");
        start = buffer.readLine();

        if (mode.equals("2")) {
          System.out.println("Provide second word: ");
          stop = buffer.readLine();
          fr = new FileReader(fileName);
          dictionary = fr.getDictionary();

          if (checkStrings(start, stop, dictionary)) {
            wl = new WordLadder(start, stop, dictionary);
            wl.tree();
          }
        } else if (mode.equals("1")) {
          fr = new FileReader(fileName);
          dictionary = fr.getDictionary();

          if (dictionary.contains(start)) {
            wl = new WordLadder(start, stop, dictionary);
            System.out.println("How many steps do you need?");

            int deepth = Integer.parseInt(buffer.readLine());

            wl.wordLadder(deepth);
          }
        }
      } catch (IOException e) {

        System.err.println("Problem with standard input!");
        System.err.println("\t" + e.getMessage());

      } //catch

    } //else if args.length

  } //main

  /**
   * Method checks if both strings are in the dictionary, if both are
   * the same word and if they have the same length.
   *
   * @param a    First word to check.
   * @param b    Second word to check.
   * @param dict Dictionary check if its contains both strings.
   *
   * @return Returns true if strings exists in dictionary, are the
   *         same
   *         length.
   */
  public static boolean checkStrings (String a, String b, ArrayList dict) {

    if (null != b) {

      if (!(a.length() == b.length())) {
        System.err.println("Words are different length!");
        return false;
      }

      if (a.equals(b)) {
        System.err.println("Words are the same!");
        return false;
      }

      if (!(dict.contains(a) && dict.contains(b))) {
        System.err.println("Dictionary does not contain the words!");
        return false;
      }
    }

    return true;
  }
} //class