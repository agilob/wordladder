/**
 *
 * @author agilob
 */
package wl;

import java.io.*;
import java.util.ArrayList;

/**
 * Class contains method to read dictionary on given path.
 *
 */
public final class FileReader {

  private FileInputStream fstream;
  private DataInputStream in;
  private BufferedReader br;
  /*
   * In this object we store words from a file.
   */
  private ArrayList dictionary = new ArrayList<>();

  /**
   * Gives access to a list of words loaded from a dictionary.
   *
   * @return A list of words from dictionary.
   */
  public ArrayList getDictionary () {

    return dictionary;
  }

  /**
   * Instance of the object from this class gives access to a
   * dictionary.
   * Dictionary is loaded from provided path.
   *
   * @param fileName Path of a file to read.
   */
  public FileReader (String fileName) {

    try {

      fstream = new FileInputStream(fileName);

      //Get the object of DataInputStream
      in = new DataInputStream(fstream);
      br = new BufferedReader(new InputStreamReader(in));

      //load file into a list
      readDictionary();
      br.close();
      in.close();

    } catch (Exception e) {//Catch exception if any
      System.err.println("Error while opening file: " + e.getMessage());
    }

  } //FileReader()

  /**
   * Reads line from a file and saves in memory in an ArrayList object
   * - dictionary. I decided to throw an exception instead catching it
   * in a while loop.
   *
   * @throws IOException In reality it doesn't throw.
   */
  private void readDictionary () throws IOException {

    String line = null;
    while ((line = br.readLine()) != null) {
      dictionary.add(line);
    } // while

  } //readDictionary
} //class
