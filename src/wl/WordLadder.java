/**
 *
 * @author agilob
 */
package wl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 * This class is constantly generating the graph whilst searching.
 * This class has all methods to generate graph, find ladder and
 * generate nodes.
 */
public class WordLadder {

  /**
   * Start word
   */
  String key = "";
  /**
   * Stop word.
   */
  String stop = "";
  /**
   * Array list to store dictionary from file.
   */
  ArrayList<String> dictionary;
  /**
   * Hashtable to store "nodes" for the graph.
   */
  Hashtable<String, ArrayList<String>> graph = new Hashtable<>();

  /**
   * Set values for instance variable of this class.
   * Creates object with main data about the graph.
   *
   * @param key        A word where the ladder starts.
   * @param stop       A word where the ladders ends.
   * @param dictionary A list of all word from a file.
   */
  public WordLadder (String key, String stop, ArrayList dictionary) {

    this.key = key;
    this.stop = stop;
    this.dictionary = dictionary;
  }

  /**
   * Sets all necessary data to find a ladder from given words and
   * print the
   * ladder.
   *
   */
  public void tree () {

    graph = generateGraph(key);

    ArrayList<String> path = generateLadder(graph, key, stop);
    int pathSize = path.size();

    if (pathSize == 0) { //if path is empty :(
      System.err.println("Ladder from: " + key + " to " + stop + " not found!");
    } else {
      //from one because first element is always null
      for (int i = 1; i < pathSize; i++) {
        System.out.println(i + ". " + path.get(i));
      }
    }

  } //wordLadder

  /**
   * Finds the ladder in provided hashtable. Stops searching when stop
   * word is found in the graph. Saves the graph in a hashtable, sends
   * it to findLadder() to reverse the path.
   *
   * @param neighbours A list of neighbours for start word.
   * @param first      String where ladder starts.
   * @param stop       String where ladder ends.
   *
   * @return Shortest path from start <i>first</i> to <i>stop</i>.
   */
  public static ArrayList<String> generateLadder (Hashtable<String, ArrayList<String>> neighbours,
                                                  String first, String stop) {

    //a hashtable to store data about previously used word
    Hashtable<String, String> previousWords = new Hashtable<>();
    Queue<String> q = new LinkedList<>();

    q.add(first); //start queue

    while (!q.isEmpty()) {

      String current = q.element(); //take first from queue, dont remove
      q.remove(); //remove taken order
      //take one word from the list
      ArrayList<String> neib = neighbours.get(current);

      if (neib != null) { //if word exists
        for (String neighbour : neib) {

          if (previousWords.get(neighbour) == null) {
            previousWords.put(neighbour, current);
            q.add(neighbour); //add new element to the queue
          }

        } //for each neighbour

      } //if ajd != null

    } //while queue is not empty

    try {
      previousWords.put(first, "");
    } catch (NullPointerException e) {
      System.out.println(e.getMessage());
    }

    return findLadder(previousWords, first, stop);
  }

  /**
   * When we know, that ladder exists, we must find it inside a
   * hashtable.
   * Method finds the ladder from end word to start in a given
   * hashtable
   *
   * @param prev   Here is the ladder we're searching on.
   * @param first  Ladder starts on this word.
   * @param second Ladder end here.
   *
   * @return Returns a list from <i>first</i> to <i>second</i>.
   */
  public static ArrayList<String> findLadder (Hashtable<String, String> prev,
                                              String first, String second) {
    ArrayList<String> result = new ArrayList<>();

    if (prev.get(second) != null) {

      for (String str = second; str != null; str = prev.get(str)) {
        result.add(str);
      }
    }

    //array list has wrong order, must be reversed
    Collections.reverse(result);

    return result;
  }

  /**
   * For each word in dictionary creates <i>first step</i> graph.
   * First layer of nodes.
   *
   * @param word Start from this word.
   *
   * @return Hashtable where keys are words from dictionary and values
   *         are
   *         lists of neighbours.
   */
  public Hashtable<String, ArrayList<String>> generateGraph (String word) {

    Hashtable<String, ArrayList<String>> neighbour = new Hashtable<>();
    int dictSize = dictionary.size() - 1; //performance puropose again...

    for (int i = 0; i < dictSize; i++) {

      //add to the hashtable key and a list of neighbours for this key
      neighbour.put(word, getNeighbours(word.toString()));
      word = dictionary.get(i); //take another word
    }

    return neighbour;

  } //generateGraph

  /**
   * Generates one way ladder of specified depth for given word.
   * The next word in the ladder is random one from list of
   * neighbours.
   *
   * @param depth Specifies how deep is the ladder.
   *
   */
  public void wordLadder (int depth) {

    ArrayList neighbourList = graph.get(key);
    Random randomGenerator = new Random();
    int dictSize = 1; //prevent nullpointerexception in rand
    String word = key;

    dictionary.remove(word); //remove start word
    depth--;

    System.out.println(word);

    for (int i = 0; i < depth; i++) {
      neighbourList = getNeighbours(word);
      dictSize = neighbourList.size();

      try {
        word = neighbourList.get(
                randomGenerator.nextInt(dictSize)).toString();
        dictionary.remove(word);
      } catch (IllegalArgumentException e) {
        System.err.println("Could not find so long ladder!");
        break;
      }


      System.out.println(word);



    } //for i

  } //tree

  /**
   * Methods returns a list of all neighbours for given string.
   * Neighbour is another word, which differs by one character from
   * the given
   * one.
   *
   * @param start Find neighbours for this word.
   *
   * @return A list of neighbours for givens string is returned.
   */
  public ArrayList getNeighbours (String start) {

    //a list of neighbours
    ArrayList<String> tmpDict = new ArrayList<>();

    // 3 ints for performace puroposes
    // do you know how expensive .size() is?
    int distance,
            length,
            dictSize = dictionary.size();

    for (int i = 0; i < dictSize; i++) {

      distance = 0;
      length = dictionary.get(i).length();

      for (int j = 0; j < length; j++) {

        //increment difference if characters are different
        if (start.charAt(j) != dictionary.get(i).charAt(j)) {
          distance++;
        }

      } //for j

      if (distance == 1) {                //if the difference is one
        tmpDict.add(dictionary.get(i)); //add to the list
      }

    } //for i

    return tmpDict;
  } //getNeighbours
} //class
